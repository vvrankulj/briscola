// Includes, namespace and prototypes
#include "Main.h"
#include "MainMenu.h"
using namespace AGK;
app App;

MainMenu mainMenu;

#ifdef IDE_ANDROID
extern void exit(int);
#endif

bool app::did_start = false;
bool app::did_end = false;

int g_gameMenu = 0;

// Begin app, called once at the start
void app::Begin( void )
{
#ifdef IDE_MAC
	// use device resolution
	agk::SetVirtualResolution(m_DeviceWidth,m_deviceHeight);
#endif
	agk::SetVirtualResolution(agk::GetDeviceWidth(), agk::GetDeviceHeight());
	mainMenu.LoadCardTables();
	mainMenu.LoadMainMenu();
	mainMenu.LoadOptionsMenu();
	mainMenu.LoadGameType();
	mainMenu.CreateBackground();
}

void MainMenuHandle(){
	mainMenu.CreateMainMenu();
	mainMenu.FadeInButtons();
	if(agk::GetVirtualButtonPressed(4)==1){
		g_gameMenu = 3;
		mainMenu.DeleteMainMenu();
	}
	if(agk::GetVirtualButtonPressed(5)){
		g_gameMenu = 1;
		mainMenu.DeleteMainMenu();
	}
	if(agk::GetVirtualButtonPressed(6)){
		mainMenu.DeleteCardTable();
		mainMenu.DeleteMainMenu();
		mainMenu.DeleteOptionsMenu();
		mainMenu.DeleteGameType();
		exit(0);
	}
}

void OptionsMenuHandle(){
	mainMenu.CreateOptionsMenu();
	mainMenu.FadeInOptionsMenu();
	if(agk::GetVirtualButtonPressed(7)==1){
		agk::Print("Settings");
	}
	if(agk::GetVirtualButtonPressed(8)==1){
		agk::Print("Card select");
	}
	if(agk::GetVirtualButtonPressed(9)==1){
		//TableSearchHandle();
		mainMenu.DeleteOptionsMenu();
		g_gameMenu = 2;
	}
	if(agk::GetVirtualButtonPressed(10)==1){
		agk::Print("Statistics");
	}
	if(agk::GetVirtualButtonPressed(11)==1){
		//MainMenuHandle();
		mainMenu.DeleteOptionsMenu();
		g_gameMenu = 0;
	}
}

void GameTypeHandle(){
	/*agk::Print(mainMenu.g_gameTypeCreated);*/
	mainMenu.CreateGameType();
	mainMenu.FadeInGameType();

	if(agk::GetVirtualButtonPressed(7)==1){
		agk::Print("Play single");
	}
	if(agk::GetVirtualButtonPressed(8)==1){
		agk::Print("Play double");
	}

	if(agk::GetVirtualButtonPressed(9)==1){
		mainMenu.DeleteGameType();
		g_gameMenu = 0;
	}
}

void TableSearchHandle(){
	mainMenu.CreateCardTables();
	mainMenu.PickCardTable();
	if(mainMenu.g_tableIndex > 0){
		agk::Print("Table selected:");
		agk::Print(mainMenu.g_tableIndex);
	}
	if(agk::GetVirtualButtonPressed(3)==1){
		//MainMenuHandle();
		mainMenu.DeleteCardTable();
		g_gameMenu=1;
	}
}



void app::Loop ( void )
{
	//mainMenu.PickCardTableWithTouch();

	app::did_start = true;
	if (app::did_end) return;

	//if (agk::GetPointerPressed())
	//{
	//	closeThisApp();
	//} else {
	//}
	switch(g_gameMenu){
	case 0:
		MainMenuHandle();
		break;
	case 1:
		OptionsMenuHandle();
		break;
	case 2:
		TableSearchHandle();
		break;
	case 3:
		GameTypeHandle();
		break;
	case 4:
		//Options menu
		break;
	}



	// sync display
	agk::Sync();
}

// Called when the app ends
void app::End ( void )
{
}

#ifdef IDE_XCODE
// Called when the app returns from being in the background
void app::appGetResumed( void )
{
	// do anything that needs to be handled after being paused
	agk::Message("We have returned!");
}
#endif

void app::closeThisApp()
{
	// indicate done
	app::did_end = true;

	// completely exit the app
#ifdef AGKWINDOWS
	PostQuitMessage(0);
#endif
#ifdef AGKIOS
	// forcing a quit in iOS is against recommended guidelines - use HOME button
	// the exit button is disabled on AGKIOS builds
	// but if you want to do so, this is the code
	agk::MasterReset();
	exit(1);
#endif
#ifdef IDE_ANDROID
	// similar to iOS, an exit button should not be done
	// but if you want to do so, this is the code
	agk::MasterReset();
	exit(0);
#endif
#ifdef IDE_MAC
	glfwCloseWindow();
#endif
#ifdef IDE_MEEGO
	g_appptr->quit();
#endif
#ifdef IDE_BADA
	// Bada platform has a HOME button to quit apps
	// but the END command can also quit a Bada App forcefully
	Application::GetInstance()->Terminate();
#endif
}
