#pragma once
#include "CardTypes.h";

class Card
{
    public:
        Card(void);
        ~Card(void);
        void SetCardType(CardType cardType);
        CardType GetCardType(void);
        void SetCardValue(int value);
        int GetCardValue();
    private:
        int p_Value;
        int p_Picture;
        CardType p_cardType;
};

