#pragma once

#include <iostream> 
#include <string>   
#include <vector>   

#include "Card.h" 
#include "Spiel.h"
#include "NetworkSettings.h"

class Player
{
    public:
        Player(void);
        ~Player(void);
        std::string GetName();
        void SetName(std::string name);
        Spiel cardsInHand;
		Spiel takenCards;
		bool isActive;
		bool isAi;
		void Reset();
        void SetNetworkSettings(NetworkSettings settings);
        NetworkSettings GetNetworkSettings();
        Spiel Memory;
        //void TakeCardFromTheDesk(Desk desk);
    private:
        std::string p_name;
        NetworkSettings p_networkSettings;
};

