#include <iostream> ;
#include <string>   ;
#include <vector>   ;
#include <algorithm> ;
#include <functional>;
#include <ctime>     ;
#include <cstdlib>   ;


#include "Spiel.h"
#include "CardTypes.h";

Spiel::Spiel(void)
{
    //std::vector<Card> this->p_cards;
    //this->p_cards = new std::vector<Card>();
    this->p_cards.clear();
}


Spiel::~Spiel(void)
{
    //this->p_cards._Destroy;
    this->p_cards.clear();
}

void Spiel::AddCardToSpiel(Card card){
    this->p_cards.push_back(card);
}

int Spiel::GetCardCount(){
    return this->p_cards.size();
}

Card Spiel::GetFirstCard(){
    int index = 0;
    Card card = this->p_cards.at(index);
    this->p_cards.erase(this->p_cards.begin());
    return card;
}

Card Spiel::GetLastCard(){
    //brisat i uzet!
    int index = this->p_cards.size();
    Card card = this->p_cards.at(index);
    //this->p_cards.erase(index);
    this->p_cards.pop_back();
    return card;
}

void Spiel::FillWithAllCards(){
    this->p_cards.clear();
    for (int i = 1; i <= 10; i++){
        Card card1;
        card1.SetCardType(CardType::BASTONI);
        card1.SetCardValue(i);
        this->AddCardToSpiel(card1);

        Card card2;
        card2.SetCardType(CardType::COPE);
        card2.SetCardValue(i);
        this->AddCardToSpiel(card2);

        Card card3;
        card3.SetCardType(CardType::SPADI);
        card3.SetCardValue(i);
        this->AddCardToSpiel(card3);

        Card card4;
        card4.SetCardType(CardType::DANARI);
        card4.SetCardValue(i);
        this->AddCardToSpiel(card4);
    }
}


void Spiel::ClearAllCards(){
    this->p_cards.clear();
}

void Spiel::MixCards(){
    random_shuffle( this->p_cards.begin(), this->p_cards.end() );
	random_shuffle( this->p_cards.begin(), this->p_cards.end() );
}

int  Spiel::GetCardIndex(Card card){
    int size = this->p_cards.size();
    for( int i = 0; i < size; i++ ) {
        Card crd = p_cards.at(i);
        if( crd.GetCardType()==card.GetCardType() && crd.GetCardValue()==card.GetCardValue())   return i;
    }
    return -1;
}

void Spiel::DeleteCardByIndex(int index){
    p_cards.erase(p_cards.begin() + index);
}

void Spiel::RemoveSpecificCard(Card card){
    int index = GetCardIndex(card);
    if (index > -1) DeleteCardByIndex(index);
}

vector<Card> Spiel::GetAllCards(){
	return this->p_cards;
}

Spiel Spiel::GetMissingCards(CardType cardType){
    Spiel cards = Spiel();
    for (int i = 1; i<=10 ; i++){
        Card card;
        card.SetCardType(cardType);
        card.SetCardValue(i);
        if (this->GetCardIndex(card)==-1) cards.AddCardToSpiel(card);
    }
    return cards;
}
