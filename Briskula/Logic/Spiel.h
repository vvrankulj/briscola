#pragma once


#include <iostream> 
#include <string>   
#include <vector>   

#include "Card.h"

using namespace std;

class Spiel
{
    public:
        Spiel(void);
        ~Spiel(void);
        void ClearAllCards();
        void AddCardToSpiel(Card card);
        void FillWithAllCards();
        void MixCards();
        Card GetFirstCard();
        Card GetLastCard();
        int  GetCardCount();
        //void ClearAllData();
        void RemoveSpecificCard(Card card);
		vector<Card> GetAllCards();
        Spiel GetMissingCards(CardType cardType);
    private:
        int GetCardIndex(Card card);
        void DeleteCardByIndex(int index);
        vector<Card> p_cards;
};

