#include "Player.h" 
using namespace std;

Player::Player(void)
{
	this->Reset();
}


Player::~Player(void)
{
    this->Reset();
}

string Player::GetName(){
    return p_name;
}

void Player::SetName(string name){
    p_name=name;
}
void Player::Reset(){
	this->isAi = false;
	this->isActive = false;
	this->SetName("");
	this->takenCards.ClearAllCards();
    this->cardsInHand.ClearAllCards();
    //this->SetNetworkSettings(NULL /* NetworkSettings()*/);
}

//void Player::TakeCardFromTheDesk(Desk desk){
//    this->cardsInHand.AddCardToSpiel(desk.cards.GetLastCard());
//}