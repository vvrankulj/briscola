#include "Briscola.h"


Briscola::Briscola(void)
{
}


Briscola::~Briscola(void)
{
}

void Briscola:: SetNumberOfComputerPlayers(int number){
    if (number+GetNumberOfHumanPlayers()>4) throw new exception("Total number of players cannot be more than 4!");
    this->p_numberOfComputers = number;
}

void Briscola:: SetNumberOfHumanPlayers(int number)  {
    if (number+GetNumberOfComputerPlayers()>4) throw new exception("Total number of players cannot be more than 4!");
    this->p_numberOfPlayers = number;
}

int Briscola:: GetNumberOfHumanPlayers(){
    return this->p_numberOfPlayers;
}

int Briscola:: GetNumberOfComputerPlayers(){
    return this->p_numberOfComputers;
}
void Briscola:: InitGame(int humanPlayers,int computerPlayers,bool singleGame){
    SetNumberOfComputerPlayers(computerPlayers);
}

int Briscola::SumCards(Spiel spiel){
    int cardCount = spiel.GetCardCount();
    int sumCards = 0;
    vector<Card> cards = spiel.GetAllCards();
    for( int i = 0; i < cardCount; i++ ) {
        switch (cards.at(i).GetCardValue()){
            case 1: sumCards += 11; break;
            case 2: sumCards += 0; break;
            case 3: sumCards += 10; break;
            case 4: sumCards += 0; break;
            case 5: sumCards += 0; break;
            case 6: sumCards += 0; break;
            case 7: sumCards += 0; break;
            case 8: sumCards += 2; break; //fanat
            case 9: sumCards += 3; break; //konj
            case 10: sumCards += 4; break; //kralj
        }
    }
    return sumCards;
}