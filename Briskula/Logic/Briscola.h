#pragma once
#include "Card.h" 
#include "Spiel.h"
class Briscola
{
public:
    Briscola(void);
    ~Briscola(void);
    void SetNumberOfComputerPlayers(int number);
    void SetNumberOfHumanPlayers(int number);
    int GetNumberOfHumanPlayers();
    int GetNumberOfComputerPlayers();
	/*
	game planing:
		1	human and 1 comp
		1	human and 2 comp
		2	human and 2 comp
		2	human and 1 comp
		1	human and 3 comp
		3	human and 1 comp
	network gaming not yet will be implemented!
	*/
    void InitGame(int humanPlayers,int computerPlayers,bool singleGame);
    void NextMove();
    void ClearData();
    Card FindBestRespond(); 
    void ShareCards();
    int SumCards(Spiel spiel);
    void inizializeDesk();
    Card playingCard;
    Spiel cards;
    Spiel fallenCards; 
private:
    int p_numberOfPlayers;
    int p_numberOfComputers;
    bool isNetworkGame;

};

        /*
            jedan ili dva usera uzimaju karte koje su bacene
            kodnjih cu stavit spielove
            http://algoritmicamente.wordpress.com/2009/11/17/briscola/#more-123
        */