#include "MainMenu.h"
#include "agk.h"
#include "Logic/CardTypes.h"

//local variables
float _alphaOffset = 3.3f;
float _alpha = 0;
int nextTableIndex = 3;
int previousTableIndex = 2;
int currentTableIndex = 2;
float tableSpeed = 22.0f;
bool updatingTablesLeft = false;
bool updatingTablesRight = false;

void UpdateTablesRight();
void UpdateTablesLeft();
void LoadTextExplainations_MainMenu();
void LoadTextExplainations_OptionsMenu();
void DeleteTextExplainations_MainMenu();
void DeleteTextExplainations_OptionsMenu();

//Global variable for table
int g_tableIndex = -1;

CardTable cardTable;

// Buttons - Card Table selector
int buttonLeftUp;
int buttonLeftDown;
int buttonRightUp;
int buttonRightDown;
int returnButtonUp;
int returnButtonDown;

//Tables
int fancyGreen; 
int fancyBlue; 
int fancyPurple; 
int tavernRed; 
int tavernGreen;

int tableFrame;
//Main menu
int mainMenuBackground;
//Buttons
int playButtonUp;
int playButtonDown;
int optionsButtonUp;
int optionsButtonDown;
int exitButtonUp;
int exitButtonDown;

//Options Menu
int settingsButtonUp;
int settingsButtonDown;
int cardButtonUp;
int cardButtonDown;
int tablesButtonUp;
int tablesButtonDown;
int statisticButtonUp;
int statisticButtonDown;

//Game type menu
int singleTypeUp;
int singleTypeDown;
int doubleTypeUp;
int doubleTypeDown;
//Buttons
int doubleBriscolaButton = 5;
int singleBriscolaButton = 6;
int returnToMainMenu = 7;


//Global variables
bool g_cardTableCreated = false;
bool g_mainMenuCreated = false;
bool g_optionsMenuCreated = false;
bool g_gameTypeCreated = false;

int playText;
int optionsText;
int exitText;
int settingsText;
int cardText;
int tableText;
int statisticText;

void MainMenu::CreateBackground(){
	agk::CreateSprite(1,mainMenuBackground);
	agk::SetSpritePositionByOffset(1, agk::GetDeviceWidth()/2, agk::GetDeviceHeight()/2);
	agk::SetSpriteColorAlpha(1,50);


}

void MainMenu::LoadMainMenu(){
	mainMenuBackground = agk::LoadImage("Images/Misc/Background.png");
	playButtonUp = agk::LoadImage("Images/Buttons/PlayButtonUp.png");
	playButtonDown = agk::LoadImage("Images/Buttons/PlayButtonDown.png");
	optionsButtonUp = agk::LoadImage("Images/Buttons/OptionsButtonUp.png");
	optionsButtonDown = agk::LoadImage("Images/Buttons/OptionsButtonDown.png");
	exitButtonUp = agk::LoadImage("Images/Buttons/ExitButtonUp.png");
	exitButtonDown = agk::LoadImage("Images/Buttons/ExitButtonDown.png");
	//font
	agk::LoadImage ( 1, "Images/Misc/custom.png" );
}

void MainMenu::LoadOptionsMenu(){
	settingsButtonUp = agk::LoadImage("Images/Buttons/SettingsButtonUp.png");
	settingsButtonDown = agk::LoadImage("Images/Buttons/SettingsButtonDown.png");
	cardButtonUp = agk::LoadImage("Images/Buttons/CardButtonUp.png");
	cardButtonDown = agk::LoadImage("Images/Buttons/CardButtonDown.png");
	tablesButtonUp = agk::LoadImage("Images/Buttons/TablesButtonUp.png");
	tablesButtonDown = agk::LoadImage("Images/Buttons/TablesButtonDown.png");
	statisticButtonUp = agk::LoadImage("Images/Buttons/StatisticButtonUp.png");
	statisticButtonDown = agk::LoadImage("Images/Buttons/StatisticButtonDown.png");
}

void MainMenu::LoadGameType(){
	singleTypeUp = agk::LoadImage("Images/Buttons/SingleBriscolaUp.png");
	singleTypeDown = agk::LoadImage("Images/Buttons/SingleBriscolaDown.png");
	doubleTypeUp = agk::LoadImage("Images/Buttons/DoubleBriscolaUp.png");
	doubleTypeDown = agk::LoadImage("Images/Buttons/DoubleBriscolaDown.png");
}

void MainMenu::LoadCardTables(){
	buttonLeftUp = agk::LoadImage("Images/Tables/ButtonLeftUp.png");
	buttonLeftDown = agk::LoadImage("Images/Tables/ButtonLeftDown.png");
	buttonRightUp = agk::LoadImage("Images/Tables/ButtonRightUp.png");
	buttonRightDown = agk::LoadImage("Images/Tables/ButtonRightDown.png");
	returnButtonUp = agk::LoadImage("Images/Buttons/ReturnButtonUp.png");
	returnButtonDown = agk::LoadImage("Images/Buttons/ReturnButtonDown.png");

	tableFrame = agk::LoadImage("Images/Tables/TableFrame.png");

	fancyGreen = agk::LoadImage("Images/Tables/FancyTableGreenSmall.png");
	fancyPurple = agk::LoadImage("Images/Tables/FancyTablePurpleSmall.png");
	fancyBlue = agk::LoadImage("Images/Tables/FancyTableBlueSmall.png");
	tavernRed = agk::LoadImage("Images/Tables/TavernTableRedSmall.png");
	tavernGreen = agk::LoadImage("Images/Tables/TavernTableGreenSmall.png");
}

void LoadTextExplainations_MainMenu(){
	playText = agk::CreateText("Start game");
	optionsText = agk::CreateText("Options menu - Select deck, table... \nview statistics ...\nchange game settings.");
	exitText = agk::CreateText("Quit game, \nbut you don't want that, don't you?");

	agk::SetTextFontImage(playText,1);
	agk::SetTextSize(playText,18);
	agk::SetTextColor(playText,174,162,162,255);
	agk::SetTextPosition(playText,250, 90);

	agk::SetTextFontImage(optionsText,1);
	agk::SetTextSize(optionsText,18);
	agk::SetTextColor(optionsText,174,162,162,255);
	agk::SetTextPosition(optionsText,250, 190);

	agk::SetTextFontImage(exitText,1);
	agk::SetTextSize(exitText,18);
	agk::SetTextColor(exitText,174,162,162,255);
	agk::SetTextPosition(exitText,250, 320);
}

void LoadTextExplainations_OptionsMenu(){

	settingsText = agk::CreateText("Change some game settings ... \nlike sound on/off ...");
	cardText = agk::CreateText("Select type of card deck ... \ntriestine, bolognese");
	tableText = agk::CreateText("Select type of table ... \nsomething fancy or maybe \ndomestic tavern table");
	statisticText = agk::CreateText("Check your achievments, \nscores, rank ...");

	agk::SetTextFontImage(settingsText,1);
	agk::SetTextSize(settingsText,18);
	agk::SetTextColor(settingsText,174,162,162,255);
	agk::SetTextPosition(settingsText,250, 80);

	agk::SetTextFontImage(cardText,1);
	agk::SetTextSize(cardText,18);
	agk::SetTextColor(cardText,174,162,162,255);
	agk::SetTextPosition(cardText,250, 170);

	agk::SetTextFontImage(tableText,1);
	agk::SetTextSize(tableText,18);
	agk::SetTextColor(tableText,174,162,162,255);
	agk::SetTextPosition(tableText,250, 250);

	agk::SetTextFontImage(statisticText,1);
	agk::SetTextSize(statisticText,18);
	agk::SetTextColor(statisticText,174,162,162,255);
	agk::SetTextPosition(statisticText,250, 350);
}

void DeleteTextExplainations_MainMenu(){
	agk::DeleteText(playText);
	agk::DeleteText(optionsText);
	agk::DeleteText(exitText);
}
void DeleteTextExplainations_OptionsMenu(){
	agk::DeleteText(settingsText);
	agk::DeleteText(cardText);
	agk::DeleteText(tableText);
	agk::DeleteText(statisticText);
}


void MainMenu::CreateMainMenu(){
	if(!g_mainMenuCreated){
		agk::AddVirtualButton(4,120, 100,70);
		agk::SetVirtualButtonImageUp(4,playButtonUp);
		agk::SetVirtualButtonImageDown(4,playButtonDown);
		agk::AddVirtualButton(5,120, 220,70);
		agk::SetVirtualButtonImageUp(5,optionsButtonUp);
		agk::SetVirtualButtonImageDown(5,optionsButtonDown);
		agk::AddVirtualButton(6,120, 340,70);
		agk::SetVirtualButtonImageUp(6,exitButtonUp);
		agk::SetVirtualButtonImageDown(6,exitButtonDown);

		agk::SetVirtualButtonAlpha(4,0);
		agk::SetVirtualButtonAlpha(5,0);
		agk::SetVirtualButtonAlpha(6,0);

		LoadTextExplainations_MainMenu();

		g_mainMenuCreated = true;
	}
}

void MainMenu::CreateGameType(){
	if(!g_gameTypeCreated){
		agk::AddVirtualButton(7,120, 100,70);
		agk::SetVirtualButtonImageUp(7,singleTypeUp);
		agk::SetVirtualButtonImageDown(7,singleTypeDown);

		agk::AddVirtualButton(8,120, 220,70);
		agk::SetVirtualButtonImageUp(8,doubleTypeUp);
		agk::SetVirtualButtonImageDown(8,doubleTypeDown);

		agk::AddVirtualButton(9,570,450,50);
		agk::SetVirtualButtonImageUp(9,returnButtonUp);
		agk::SetVirtualButtonImageDown(9,returnButtonDown);

		agk::SetVirtualButtonAlpha(7,0);
		agk::SetVirtualButtonAlpha(8,0);

		g_gameTypeCreated = true;
	}
}


void MainMenu::CreateCardTables(){
	if(!g_cardTableCreated){
		agk::AddVirtualButton(1,20, 240,50);
		agk::SetVirtualButtonImageUp(1,buttonLeftUp);
		agk::SetVirtualButtonImageDown(1,buttonLeftDown);
		agk::AddVirtualButton(2,620, 240,50);
		agk::SetVirtualButtonImageUp(2,buttonRightUp);
		agk::SetVirtualButtonImageDown(2,buttonRightDown);
		agk::AddVirtualButton(3,570,450,50);
		agk::SetVirtualButtonImageUp(3,returnButtonUp);
		agk::SetVirtualButtonImageDown(3,returnButtonDown);

		//Tables
		agk::CreateSprite(2, fancyGreen);
		agk::CreateSprite(3, fancyPurple);
		agk::CreateSprite(4, fancyBlue);
		agk::CreateSprite(5, tavernRed);
		agk::CreateSprite(6, tavernGreen);

		//agk::SetSpriteGroup(1,1);
		agk::SetSpriteGroup(2,1);
		agk::SetSpriteGroup(3,1);
		agk::SetSpriteGroup(4,1);
		agk::SetSpriteGroup(5,1);
		agk::SetSpriteGroup(6,1);

		agk::SetSpritePosition(2,agk::GetSpriteX(1),agk::GetSpriteY(1));
		agk::SetSpritePosition(3,agk::GetSpriteX(1) + agk::GetDeviceWidth(),agk::GetSpriteY(1));
		agk::SetSpritePosition(4,agk::GetSpriteX(1) + agk::GetDeviceWidth(),agk::GetSpriteY(1));
		agk::SetSpritePosition(5,agk::GetSpriteX(1) + agk::GetDeviceWidth(),agk::GetSpriteY(1));
		agk::SetSpritePosition(6,agk::GetSpriteX(1) + agk::GetDeviceWidth(),agk::GetSpriteY(1));

		agk::SetSpriteScaleByOffset(2,0.8,0.8);
		agk::SetSpriteScaleByOffset(3,0.8,0.8);
		agk::SetSpriteScaleByOffset(4,0.8,0.8);
		agk::SetSpriteScaleByOffset(5,0.8,0.8);
		agk::SetSpriteScaleByOffset(6,0.8,0.8);

		g_cardTableCreated = true;
	}
}

void MainMenu::CreateOptionsMenu(){
	if(!g_optionsMenuCreated){
		agk::AddVirtualButton(7,120, 100,70);
		agk::SetVirtualButtonImageUp(7,settingsButtonUp);
		agk::SetVirtualButtonImageDown(7,settingsButtonDown);
		agk::AddVirtualButton(8,120, 190,70);
		agk::SetVirtualButtonImageUp(8,cardButtonUp);
		agk::SetVirtualButtonImageDown(8,cardButtonDown);
		agk::AddVirtualButton(9,120, 280,70);
		agk::SetVirtualButtonImageUp(9,tablesButtonUp);
		agk::SetVirtualButtonImageDown(9,tablesButtonDown);
		agk::AddVirtualButton(10,120, 370,70);
		agk::SetVirtualButtonImageUp(10,statisticButtonUp);
		agk::SetVirtualButtonImageDown(10,statisticButtonDown);
		agk::AddVirtualButton(11,570,450,50);
		agk::SetVirtualButtonImageUp(11,returnButtonUp);
		agk::SetVirtualButtonImageDown(11,returnButtonDown);

		agk::SetVirtualButtonAlpha(3,0);
		agk::SetVirtualButtonAlpha(7,0);
		agk::SetVirtualButtonAlpha(8,0);
		agk::SetVirtualButtonAlpha(9,0);
		agk::SetVirtualButtonAlpha(11,0);

		LoadTextExplainations_OptionsMenu();

		g_optionsMenuCreated = true;
	}
}

void MainMenu::DeleteMainMenu(){
	if(g_mainMenuCreated){
		_alpha = 0;	
		agk::DeleteVirtualButton(4);
		agk::DeleteVirtualButton(5);
		agk::DeleteVirtualButton(6);
		DeleteTextExplainations_MainMenu();
		g_mainMenuCreated = false;
	}
}

void MainMenu::DeleteOptionsMenu(){
	if(g_optionsMenuCreated){
		_alpha = 0;	
		agk::DeleteVirtualButton(7);
		agk::DeleteVirtualButton(8);
		agk::DeleteVirtualButton(9);
		agk::DeleteVirtualButton(10);
		agk::DeleteVirtualButton(11);
		DeleteTextExplainations_OptionsMenu();
		g_optionsMenuCreated = false;
	}
}

void MainMenu::DeleteGameType(){
	if(g_gameTypeCreated){
		_alpha = 0;
		agk::DeleteVirtualButton(7);
		agk::DeleteVirtualButton(8);
		agk::DeleteVirtualButton(9);
		g_gameTypeCreated = false;
	}
}

void MainMenu::DeleteCardTable(){
	if(g_cardTableCreated){
		_alpha = 0;	
		//agk::DeleteSprite(1);
		agk::DeleteSprite(2);
		agk::DeleteSprite(3);
		agk::DeleteSprite(4);
		agk::DeleteSprite(5);
		agk::DeleteSprite(6);
		//agk::DeleteSprite(7);

		agk::DeleteVirtualButton(1);
		agk::DeleteVirtualButton(2);
		agk::DeleteVirtualButton(3);

		g_cardTableCreated = false;
	}
}


void MainMenu::PickCardTable(){
	if(currentTableIndex!=6){
		agk::SetVirtualButtonVisible(1,100);
	}else{
		agk::SetVirtualButtonVisible(1,0);
	} 

	if(currentTableIndex!=2){
		agk::SetVirtualButtonVisible(2,100);
	}else{
		agk::SetVirtualButtonVisible(2,0);
	}

	if(!updatingTablesLeft){
		if(agk::GetVirtualButtonPressed(1) == 1 && currentTableIndex!=6){
			updatingTablesLeft = true;
		}
	}else{
		UpdateTablesLeft();
	}

	if(!updatingTablesRight){
		if(agk::GetVirtualButtonPressed(2) == 1 && currentTableIndex!=2){
			updatingTablesRight = true;
		}
	}else{
		UpdateTablesRight();
	}

	if(!updatingTablesLeft || !updatingTablesRight){
		if (agk::GetPointerPressed() && agk::GetSpriteHitTest(currentTableIndex,agk::GetPointerX() , agk::GetPointerY()) == 1)
		{
			g_tableIndex = currentTableIndex;
		}
	}
}


void UpdateTablesLeft(){
	if(agk::GetSpriteX(nextTableIndex) > 80){
		agk::SetSpritePosition(currentTableIndex,agk::GetSpriteX(currentTableIndex)-tableSpeed, agk::GetSpriteY(currentTableIndex));
		agk::SetSpritePosition(nextTableIndex,agk::GetSpriteX(nextTableIndex)-tableSpeed, agk::GetSpriteY(nextTableIndex));
	}else{
		updatingTablesLeft = false;
		previousTableIndex = currentTableIndex;
		currentTableIndex = nextTableIndex;
		nextTableIndex ++;
	}
}

void UpdateTablesRight(){
	if(agk::GetSpriteX(previousTableIndex) <= 65){
		agk::SetSpritePosition(currentTableIndex,agk::GetSpriteX(currentTableIndex)+tableSpeed, agk::GetSpriteY(currentTableIndex));
		agk::SetSpritePosition(previousTableIndex,agk::GetSpriteX(previousTableIndex)+tableSpeed, agk::GetSpriteY(previousTableIndex));
	}else{
		updatingTablesRight = false;
		nextTableIndex = currentTableIndex;
		currentTableIndex = previousTableIndex;
		previousTableIndex --;
	}
}

void MainMenu::FadeInButtons(){
	agk::SetVirtualButtonAlpha(4,_alpha);
	agk::SetVirtualButtonAlpha(5,_alpha);
	agk::SetVirtualButtonAlpha(6,_alpha);
	_alpha = _alpha + _alphaOffset;
}
void MainMenu::FadeInOptionsMenu(){
	agk::SetVirtualButtonAlpha(7,_alpha);
	agk::SetVirtualButtonAlpha(8,_alpha);
	agk::SetVirtualButtonAlpha(9,_alpha);
	agk::SetVirtualButtonAlpha(10,_alpha);
	agk::SetVirtualButtonAlpha(11,_alpha);
	_alpha = _alpha + _alphaOffset;
}

void MainMenu::FadeInGameType(){
	agk::SetVirtualButtonAlpha(7,_alpha);
	agk::SetVirtualButtonAlpha(8,_alpha);
	_alpha = _alpha + _alphaOffset;
}


