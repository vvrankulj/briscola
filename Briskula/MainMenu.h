
struct CardTable{
	int CardTableSprite;
	int CardImage;
};

class MainMenu
{
public:
	void CreateBackground(void);
	void FadeInSplash(void);
	void FadeOutSplash(void);
	void LoadCardTables(void);
	void CreateCardTables(void);
	void DeleteCardTable(void);
	void PickCardTable(void);
	void PickCardTableWithTouch(void);
	void LoadMainMenu(void);
	void CreateMainMenu(void);
	void FadeInButtons(void);
	void DeleteMainMenu(void);
	void LoadOptionsMenu(void);
	void CreateOptionsMenu(void);
	void FadeInOptionsMenu(void);
	void DeleteOptionsMenu(void);
	void LoadGameType(void);
	void CreateGameType(void);
	void FadeInGameType(void);
	void DeleteGameType(void);
	bool g_cardTableCreated;
	bool g_mainMenuCreated;
	bool g_optionsMenuCreated;
	bool g_gameTypeCreated;
	int g_tableIndex;


private:
	/*void UpdateTables(void);*/

};



