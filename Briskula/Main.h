#ifndef _H_APP
#define _H_APP

// Link to AGK libraries
#include "agk.h"

/////////////////////////////////////////
// CLASS DEFINITION /////////////////////
/////////////////////////////////////////
class app
{
	public:
		// static attributes
		static bool did_start;
		static bool did_end;

	public:
		// main vars
#ifdef IDE_MAC
		unsigned int m_DeviceWidth;
		unsigned int m_DeviceHeight;
#endif
	
	public:
		// constructor
		app() {memset ( this, 0, sizeof(app));}
	
		// main app functions
		void Begin( void );
		void Loop( void );
		void End( void );
#ifdef IDE_XCODE
		void appGetResumed( void );
#endif

	private:
		void closeThisApp();
};

extern app App;

#endif

// Allow us to use the LoadImage function name
#ifdef LoadImage
#undef LoadImage
#endif
